# parsehelper
module ParseHelper
  require 'json'
  require 'json-schema'
  require 'file_helper'

  # Parses and validates API request
  class ApiRequestParser
    def initialize(schema_file)
      puts "Using API schema #{schema_file}"
      @schema_file = schema_file
      file = FileHelper.open_read(@schema_file)
      @schema = JSON.parse(File.read(file))
      file.close
    rescue StandardError => e
      warn e.message
    end

    def parse_request(str)
      data = JSON.parse(str)
      raise "#{data} not according to #{@schema_file}" \
      unless JSON::Validator.validate(@schema, data)

      data
    rescue StandardError => e
      warn e.message
    end
  end

  require 'date'
  def self.parse_date(dd_mm_yyyy)
    Date.strptime(dd_mm_yyyy, '%d.%m.%Y')
  rescue StandardError => e
    warn e.message
  end
end
