# frozen_string_literal: true

$LOAD_PATH.unshift(__dir__) unless $LOAD_PATH.include?(__dir__)
# FIXME: not elegant, but perhaps readable way
$LOAD_PATH << './lib'
$LOAD_PATH << './lib/courses'
$LOAD_PATH << './lib/students'
$LOAD_PATH << './vendor/bundle/**/*'
require 'database'
require 'student'
require 'course'
require 'courses'
require 'students'
require 'parse_helper'
require 'business_logic'
require 'date'
# Hack to parse custom command line options first and then require Sinatra.
#
# Sinatra uses command line options also and exits with exception when
# unknown option is provided.
config = BusinessLogic.api_server_configuration
parser = config[:parser]

require 'sinatra'
require 'sinatra/base'
require 'sinatra/activerecord'
require 'sinatra/namespace'
set :port, config[:port]
set :bind, config[:host]
register Sinatra::ActiveRecordExtension

# rubocop: disable Metrics/BlockLength
namespace config[:namespace] do
  get '/students/?' do
    content_type :json
    @students = Student.all
    @students.to_json
  end

  get '/courses/?' do
    content_type :json
    @courses = Course.all
    @courses.to_json
  end

  post '/enrol' do
    error 400 unless (post_data = parser.parse_request(request.body.read))
    error 400 unless (course = Courses.find_course(post_data.dig('course',
                                                                 'id')))
    error 400 unless (student = Students.find_student(post_data.dig('student',
                                                                    'name')))
    error 400 unless course.enrol(student)
  end

  post '/grade' do
    error 400 unless (post_data = parser.parse_request(request.body.read))
    error 400 unless (course = Courses.find_course(post_data.dig('course',
                                                                 'id')))
    error 400 unless (student = Students.find_student(post_data.dig('student',
                                                                    'name')))
    grade = post_data.dig('grade', 'number')

    grade_date = post_data.dig('grade', 'date')

    error 400 unless course.grade_student(student, grade, grade_date)
  end
end
# rubocop: enable Metrics/BlockLength
