# frozen_string_literal: true

require 'business_logic'

options = ArgumentsParser.parse_cli_program_arguments
BusinessLogic.cli_program_business_logic(options)

# Should never get here
warn 'WOW!!! How did you end up here? This should newer happen.', ''
abort
