require 'active_record'
# class student
class Student < ActiveRecord::Base
  def to_s
    "#{name} (id: #{id})"
  end
end
